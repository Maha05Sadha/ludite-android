package com.absyz.ludite.inject.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

/**
 * Created by Ayush on 28/06/2020.
 */

public class CustomButton extends AppCompatButton {

    public CustomButton(Context context) {
        super(context);
        setFont();
    }

    public CustomButton(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }

    public CustomButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont();
    }

    private void setFont() {
        Typeface normalTypeface = Typeface.createFromAsset(getContext().getAssets(), constant.FONT_REGULAR);
        setTypeface(normalTypeface);
    }

}
