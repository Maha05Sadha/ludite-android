package com.absyz.ludite.inject.module;

import dagger.Module;

/**
 * Created by Ayush on 26-05-2020.
 */

@Module(includes = ViewModelModule.class)
public class ApplicationModule {
}
