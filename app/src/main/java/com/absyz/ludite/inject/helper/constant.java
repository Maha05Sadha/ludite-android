package com.absyz.ludite.inject.helper;

public interface constant {
    String FONT_REGULAR = "fonts/Roboto-Regular.ttf";
    String FONT_ROBOTO_MEDIUM = "fonts/Roboto-Medium.ttf";
    String FONT_BOLD = "fonts/Roboto-Bold.ttf";
    String FONT_THIN = "fonts/Roboto-Light.ttf";
    String FONT_LIGHT = "fonts/Roboto-Regular.ttf";
}
