package com.absyz.ludite.inject;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import androidx.multidex.MultiDexApplication;

import com.absyz.ludite.BuildConfig;
import com.absyz.ludite.inject.component.ApplicationComponent;
import com.absyz.ludite.inject.component.DaggerApplicationComponent;
import com.absyz.ludite.inject.module.NetworkModule;

/**
 * Created by Ayush on 26-05-2020.
 */

public class ApplicationController  extends MultiDexApplication {

    private final static String TAG = "ApplicationController";
    private static Context context;
    private ApplicationComponent mApplicationComponent;

    public static Context getContext() {
        return context;
    }

    public static ApplicationController getApp(Activity activity) {
        return (ApplicationController) activity.getApplication();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "onCreate");

        context = this;

        // Dagger%COMPONENT_NAME%
        mApplicationComponent = DaggerApplicationComponent.builder()
                // list of modules that are part of this component need to be created here too
                .networkModule(new NetworkModule(BuildConfig.BASE_URL))
                .applicationModule(new ApplicationModule(this)) // This also corresponds to the name of your module: %component_name%Module
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.e(TAG, "onTerminate");
    }
}
