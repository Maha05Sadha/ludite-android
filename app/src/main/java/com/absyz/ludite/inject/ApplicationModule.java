package com.absyz.ludite.inject;

import android.app.Application;

import com.absyz.ludite.inject.module.ViewModelModule;

import dagger.Module;


/**
 * Created by Ayush on 26-05-2020.
 */

@Module(includes = ViewModelModule.class)
public class ApplicationModule {

    Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }


}
