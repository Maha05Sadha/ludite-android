package com.absyz.ludite.inject.helper;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import androidx.annotation.RequiresApi;
import com.google.android.material.textfield.TextInputLayout;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Ayush 28/05/2020.
 */

public class CustomTextInputLayout extends TextInputLayout {

    private Object collapsingTextHelper;
    private Rect bounds;
    private Method recalculateMethod;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public CustomTextInputLayout(Context context) {
        this(context, null);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public CustomTextInputLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public CustomTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        adjustBounds();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void init() {
        try {
            Field cthField = TextInputLayout.class.getDeclaredField("mCollapsingTextHelper");
            cthField.setAccessible(true);
            collapsingTextHelper = cthField.get(this);

            Field boundsField = collapsingTextHelper.getClass().getDeclaredField("mCollapsedBounds");
            boundsField.setAccessible(true);
            bounds = (Rect) boundsField.get(collapsingTextHelper);

            recalculateMethod = collapsingTextHelper.getClass().getDeclaredMethod("recalculate");
        } catch (NoSuchFieldException | IllegalAccessException | NoSuchMethodException e) {
            collapsingTextHelper = null;
            bounds = null;
            recalculateMethod = null;
            //e.printStackTrace();
        }
    }

    private void adjustBounds() {
        if (collapsingTextHelper == null) {
            return;
        }

        try {
            bounds.left = getEditText().getLeft() + getEditText().getPaddingLeft();
            recalculateMethod.invoke(collapsingTextHelper);
        } catch (InvocationTargetException | IllegalAccessException | IllegalArgumentException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    /*@Override
    public void setHintAnimationEnabled(boolean enabled) {
        super.setHintAnimationEnabled(enabled);
        setHintAnimationEnabled(true);
    }

    @Override
    public void setHintEnabled(boolean enabled) {
        super.setHintEnabled(enabled);
        setHintEnabled(true);
    }*/

    @Override
    public void setErrorEnabled(boolean enabled) {
        super.setErrorEnabled(enabled);
        if (!enabled) {
            return;
        }

        try {
            Field errorViewField = TextInputLayout.class.getDeclaredField("mErrorView");
            errorViewField.setAccessible(true);
            TextView errorView = (TextView) errorViewField.get(this);
            if (errorView != null) {
                errorView.setGravity(Gravity.END);
                errorView.setGravity(Gravity.RIGHT);
                LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                params.gravity = Gravity.END;
                errorView.setLayoutParams(params);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
