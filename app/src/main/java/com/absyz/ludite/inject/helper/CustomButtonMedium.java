package com.absyz.ludite.inject.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

public class CustomButtonMedium extends AppCompatButton {

    public CustomButtonMedium(Context context) {
        super(context);
        setFont();
    }

    public CustomButtonMedium(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }

    public CustomButtonMedium(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont();
    }

    private void setFont() {
        Typeface normalTypeface = Typeface.createFromAsset(getContext().getAssets(), constant.FONT_ROBOTO_MEDIUM);
        setTypeface(normalTypeface);
    }

}
