package com.absyz.ludite.inject.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;


public class CustomRobotRegular extends AppCompatTextView {

    public CustomRobotRegular(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public CustomRobotRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public CustomRobotRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface normalTypeface = Typeface.createFromAsset(getContext().getAssets(), constant.FONT_REGULAR);
        this.setTypeface(normalTypeface);
        this.setLineSpacing(0.1f,1.0f);
    }
}




