package com.absyz.ludite.retrofit;


/**
 * Created by Ayush on 26-05-2020.
 */
public enum ApiStatus {
    LOADING,
    SUCCESS,
    ERROR
}