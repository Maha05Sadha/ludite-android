package com.absyz.ludite.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.absyz.ludite.MainActivity;
import com.absyz.ludite.R;
import com.absyz.ludite.databinding.SplashActivityBinding;


/**
 * Created by Ayush on 26-05-2020.
 */

public class SplashActivity extends AppCompatActivity {

    private MyViewPagerAdapter myViewPagerAdapter;
    private TextView[] dots;
    private int[] layouts;

    /*activity dataBinding*/
    SplashActivityBinding splashActivityBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        splashActivityBinding = DataBindingUtil.setContentView(SplashActivity.this, R.layout.splash_activity);

        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }

        // layouts of welcome sliders
        layouts = new int[]{
                R.layout.intro_slide_one,
                R.layout.intro_slide_two,
                R.layout.intro_slide_three

        };

        // adding bottom dots  
        addBottomDots(0);
        // making notification bar transparent  
//        changeStatusBarColor(R.color.transparentFull);
        changeStatusBarColor(R.color.dot_dark_screen2);

        myViewPagerAdapter = new MyViewPagerAdapter();
        splashActivityBinding.viewPager.setAdapter(myViewPagerAdapter);
        splashActivityBinding.viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        splashActivityBinding.btnSkip.setOnClickListener(v -> {
            launchLoginScreen();
        });

        splashActivityBinding.btnNext.setOnClickListener(v -> {
            // checking for last page if true launch MainActivity  
            int current = getItem(+1);
            if (current < layouts.length) {
                // move to next screen  
                splashActivityBinding.viewPager.setCurrentItem(current);
            } else {
                launchLoginScreen();
            }
    });
    }

    private int getItem(int i) {
        return splashActivityBinding.viewPager.getCurrentItem() + i;
    }


    private void launchLoginScreen() {
        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
    }


    // Making notification bar transparent
    private void changeStatusBarColor(int colorid) {
        Window window =  getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            window.setStatusBarColor(getResources().getColor(colorId));
            window.setStatusBarColor(getResources().getColor(R.color.dot_dark_screen2));
        }
    }


    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int colorsActive = getResources().getColor(R.color.array_dot_active);
        int colorsInactive = getResources().getColor(R.color.array_dot_inactive);

        splashActivityBinding.layoutDots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive);
            splashActivityBinding.layoutDots.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive);
    }

    //  viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            // changing the next button text 'NEXT' / 'Sign In'
            if (position == layouts.length - 1) {
                // last page. make button text to GOT IT
                splashActivityBinding.btnNext.setText(getString(R.string.sign_in_text));
                splashActivityBinding.btnSkip.setVisibility(View.GONE);
            } else {
                // still pages are left
                splashActivityBinding.btnNext.setText(getString(R.string.next));
                splashActivityBinding.btnSkip.setVisibility(View.VISIBLE);
            }
        }
        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

}
